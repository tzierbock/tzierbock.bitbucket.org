function update() {
	// set the size of the canvas
	var width = document.getElementById('map-container').style.width;

	// debug
	// window.alert(width.toString());

	var canvas = document.getElementById('map');
	canvas.setAttribute('width', 1200);
	canvas.setAttribute('height', 700);

	var ctx = canvas.getContext('2d');

	//ctx.scale(0.5,0,5);
	ctx.beginPath();
	ctx.moveTo(0,0);
	ctx.lineTo(1200, 700);
	ctx.strokeStyle = '#000'
	ctx.stroke();
}


// Initial setup when the page is loaded
function load_map() {
	var canvas = document.getElementById('map');

	canvas.style.backgroundColor = "#39407F";
	update();
}