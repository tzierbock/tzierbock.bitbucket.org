#!/usr/bin/python

# DXF converter
#
# Description:
# This module converts DXF files, placed in the map folder
# to CSV files, containing geometric informations for all
# entities within the DXF file.
#
# Author:
# Tom Zierbock - 2014
# All rights reserved

# Import all basic libs we need
import os
import sys
import uuid

# Lib for reading DXF files
import dxfgrabber as grabber

# Helper functions
from chipmunk import *
from building import Building

# Some design rules
# Directory containing the maps
mapDir = '/Volumes/Volume 1/Algoshop/ASD/maps/'




# Each building is represented as a rectangle
# this function extracts their coordinats and
# places them in a CSV

# CSV format (with example):
# 
# |	Buidling id	|  centre	|  orientation	|	width	|  length	|  height	|
# -------------------------------------------------------------------------------
# |	  (UUID)	| 45.8,78.7	|      58		|     18	|     7		|     6 	|
# Note: The height varibale is set to a default value

def extractBuildings(file_name):
	# Try opening DXF file
	dxfF = None
	error = None


	try:
		print "Opening ", file_name
		dxfF = grabber.readfile(file_name)
	except Exception, e:
		# Give user feedback
		print "Issue opening DXF file."

		# Signal failure
		return False

	# Get all objects from the file
	entities = [ house for house in dxfF.entities if house.dxftype == 'LWPOLYLINE']

	# Check if the file is empty
	if len(entities) >= 1:
		buildings = []

		# Loop through all objects
		for house in entities:
			newB = Building()

			# Set unique identifyer
			newB.titleId = uuid.uuid4()

			# extract coordinats
			points = house.points

			# calculate center
			newB.location = center(points[0], points[2])

			# calculate orientation
			# The first two points define the primary edge
			newB.orientation = orientation(points[0], points[1])

			
			sideA = hyp(points[0], points[1])
			sideB = hyp(points[1], points[2])

			# set width (smaller side) and length
			newB.width = min(sideA, sideB)
			newB.length = max(sideA, sideB)

			# set default height of 6m
			newB.height = 6

			buildings.append(newB)

		return buildings

	else:
		# inform the user there where no rectangles in the file
		print "No buildings recognised in file."
		return False

# Takes a Building onject and generates a CSV file for it
def generateCSV(buildings, directory, file_name):

	f = open(directory + file_name + '.csv', 'w')
	
	for b in buildings:
		newLine = "{0}, {1}, {2}, {3}, {4}, {5}\n".format(str(b.titleId), str(b.location), str(b.orientation), str(b.width), str(b.length), str(b.height))
		f.write(newLine)

	f.close()


# Find any DXF file in the foler that has not
# got a corresponding CSV
def checkForNewDXF(directory):
	newDXFs = []

	# Get all dxf files in directory
	files = [f for f in os.listdir(directory) if f.endswith('dxf')]

	if len(files) > 0:
		for f in files:

			if not os.path.exists(directory + f + '.csv'):
				newDXFs.append(f)


	if len(newDXFs) > 0:
		return newDXFs
	else:
		return False



# Where all the fun starts
def main():
	
	# Check if directory exists
	if os.path.exists(mapDir):
		
		# Check for any new DXF files
		newDXFs = checkForNewDXF(mapDir)

		if newDXFs:
			for dxf in newDXFs:
				buildings = extractBuildings(mapDir + dxf)
				generateCSV(buildings, mapDir, dxf)

			print "Added CSVs for " + str(len(newDXFs)) + " file(s)"

		else:
			print "No new files found"

	else:
		print "Error: \"" + mapDir + "\" does not exists" 


# Call module if runn on its own
if __name__ == '__main__':
	main()