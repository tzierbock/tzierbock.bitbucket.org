#!/usr/bin/python

# Building object
#
# Description:
# This class describes all the 
# building parameters, necessary
# for the project.
#
# Author:
# Tom Zierbock - 2014
# All rights reserve

class Building(object):
	titleId = None

	location = (0,0)
	orientation = 0
	width = 1
	length = 1
	height = 1
