#!/usr/bin/python

# Helper functions for elephant.py
#
# Description:
# These are some helper functions for maths
# and other purposes.
#
# Author:
# Tom Zierbock - 2014
# All rights reserved

# since we are going to be doing some maths
import math

# Function that takes two tubles and calculates
# the mid point
def center(pos1, pos2):

	midX = (pos1[0] + pos2[0]) / 2
	midY = (pos1[1] + pos2[1]) / 2

	# pack into tuble
	return (midX, midY)

# Calculates orientation based on the coordinats of the primary edgemm
def orientation(pos1, pos2):
	# calc gradient
	m = (pos1[1] - pos2[1]) / (pos1[0] - pos2[0])
	
	# calc angle between tangent and x-axis
	alpha = math.degrees(math.atan(-1/m) if m !=0 else 0)
	return alpha

# Calc hypotenuse
def hyp(p1, p2):
	return math.sqrt(abs(p1[0] - p2[0])**2 + abs(p1[1] - p2[1])**2)
