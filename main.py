#!/usr/bin/python

# ASD Constructor
#
# Description:
# This module runns the main webserver using
# flask as the server engine. Arguments can
# be passed at startup to define behaviour.
#
# Author:
# Tom Zierbock - 2014
# All rights reserved

# basic imports
import os
import sys

# import flask and all required modules
from flask import Flask, rquest, session, g, redirect, url_for, abort, render_template, flash

# starts the webserver
def startService():
	pass

# builds MySQL db
def buildDB():
	pass

# called on startup
def main():
	# get arguments passed to module
	args = sys.args

	# configuration dictionary
	# this dictionaries stores all the configs that are given by the user
	config = {
		"build_db": False,
		"run_service": False,
		"update_maps": False
	}

	# Loop through all options and set corresponding variables
	# This way it doesn't matter which order arguments are given
	for option in args:

		if option == "build-db":
			config["build_db"] = True

		elif option == "start":
			config["run_service"] = True

		elif option == "update-maps":
			config["update_maps"] = True






# call main when module is run on its own
if __name__ == '__main__':
	main()